
cache_path = ARGV[0]
delay = ARGV[1].to_f

def measure(path)
  Dir["cache/*.jpg"].size  
end

while true
  size1 = measure(cache_path)
  sleep delay
  size2 = measure(cache_path)

  delta_size = (size2 - size1).to_f

  printf "%5d/%5d (%3.2f / %2.2f s; %3.2f / %2.2f s)\n", size2, 50000, delta_size, delay, delta_size / delay, 1.0
end