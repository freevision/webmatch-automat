require 'sinatra'
require 'faraday'
require 'nokogiri'
require 'haml'

require 'base64'
require 'json'
require 'open-uri'

require 'timeout'
require 'pry'

class Answer < Struct.new(:answer_hash, :image_data)
end

class Level < Struct.new(:level, :lives, :domain, :answers, :start_time, :correct_answer, :last_lives)
end

class Webmatch

  attr_accessor :conn
  attr_accessor :start_time

  def initialize
    @conn = Faraday.new(url: base_url) do |f| # , proxy: 'http://127.0.0.1:3128'
      f.request  :url_encoded             # form-encode POST params
      #f.response :logger                  # log requests to STDOUT
      f.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      f.headers = {
        'Cookie' => "PHPSESSID=#{ENV['PHPSESSID'].to_s}; 6b42c0254ddb4a1d3101aa90b37620ea=34e0558827cda5790760c94dca6d8b7fe16cc404a%3A4%3A%7Bi%3A0%3Bs%3A11%3A%22everyone_sk%22%3Bi%3A1%3Bs%3A11%3A%22everyone_sk%22%3Bi%3A2%3Bi%3A7200%3Bi%3A3%3Ba%3A11%3A%7Bs%3A2%3A%22id%22%3Bs%3A5%3A%2217386%22%3Bs%3A8%3A%22username%22%3Bs%3A11%3A%22everyone_sk%22%3Bs%3A5%3A%22email%22%3BN%3Bs%3A7%3A%22groupId%22%3Bs%3A1%3A%222%22%3Bs%3A9%3A%22groupName%22%3Bs%3A8%3A%22everyone%22%3Bs%3A9%3A%22hierarchy%22%3Bs%3A10%3A%220000017386%22%3Bs%3A8%3A%22parentId%22%3Bs%3A1%3A%220%22%3Bs%3A9%3A%22countryId%22%3Bs%3A1%3A%221%22%3Bs%3A8%3A%22vatPayer%22%3Bi%3A1%3Bs%3A3%3A%22vat%22%3Bs%3A5%3A%2220.00%22%3Bs%3A8%3A%22currency%22%3Bs%3A3%3A%22EUR%22%3B%7D%7D; __utma=44628770.1528148577.1346332959.1346332959.1346332959.1; __utmb=44628770.12.10.1346332959; __utmc=44628770; __utmz=44628770.1346332959.1.1.utmcs",
        'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.82 Safari/537.1.'
      }
    end


    @brain = {}
    @brain_path = "./brain.js"
    @last_lives = 0
    load_brain!

    reset_time!
  end

  def load_brain!
    unless File.exist?(@brain_path)
      save_brain!
    end
    @brain = JSON.parse(File.read(@brain_path))
  end

  def save_brain!
    File.write(@brain_path, @brain.to_json)
  end

  def next_level!
    begin
      Timeout::timeout(2) do
        match = @conn.get(match_url)
        html = Nokogiri::HTML(match.body)
        domain = html.search("#domainBoxLoading h1").children.last.text

        answer_hashes = html.search("#domainBox img").collect do |img|
          uri = URI.parse(img.attribute("src").value)
          uri.query.split(/[=\.]/)[1]
        end

        answers = answer_hashes.map do |answer_hash|
          data = ""
          if (File.exist?(local_answer_cache_path(answer_hash)))
            data = File.read(local_answer_cache_path(answer_hash))
          else
            data = get_remote_image_data(answer_hash)
            File.write(local_answer_cache_path(answer_hash), data)
          end
          Answer.new(answer_hash, Base64.encode64(data))
        end.compact

        level = html.search("#level").text.strip.to_i
        lives = html.search("#stats-zivoty .lives img").size

        l = Level.new
        l.level = level
        l.lives = lives
        l.domain = domain
        l.answers = answers
        l.start_time = @start_time
        l.last_lives = @last_lives

        if (l.last_lives != @last_lives)
          puts "!!!! ZOBRANY ZIVOT !!!!"
        end

        l
      end
    rescue Timeout::Error
      next_level!
    end
  end

  def next_level_with_cache!
    puts "+++ next_level_with_cache!"
    level = next_level!
    if have_cached_domain?(level.domain)
      puts "+++ next_level_with_cache! loop"
      return level if level.level >= 98
      answer = level.answers.select do |answer|
        domain_data = Base64.encode64(get_local_image_data(level.domain))
        answer_data = answer.image_data

        domain_data == answer_data
      end.first

      puts "+++ next_level_with_cache! loop parse"
      unless answer.nil?
        puts "We have cached domain #{level.domain}!"
        level.correct_answer = answer.answer_hash
        
        # answer!(level.domain, answer.answer_hash, level.level)
      end

      puts "+++ next_level_with_cache! loop return"
      return level
    end
    puts "--- next_level_with_cache!"
    level
  end

  def reset!
    @conn.get reset_url
    reset_time!
  end

  def reset_time!
    puts "Resetting time!"
    @start_time = Time.now
  end

  def get_remote_image_data(answer_hash)
    data = get_screenshot(answer_hash)
  end

  def get_local_image_data(domain)
    data = File.read(local_domain_cache_path(domain))
  end

  def get_screenshot(answer)
    @conn.get(screenshot_url(answer)).body
  end

  def local_domain_cache_path(domain)
    "cache/#{domain}.jpg"
  end

  def local_answer_cache_path(answer)
    "cache/answers/#{answer}.jpg"
  end

  def have_cached_domain?(domain)
    File.exist?(local_domain_cache_path(domain))
  end

  def answer!(domain, answer, lives)
    puts "+++ Answering #{domain}"
    result = @conn.get(answer_url(answer))
    html = Nokogiri::HTML(result.body)
    win_img = html.search("#webmatchAjaxRefresh #domainBox a.win img").first

    correct_answer = answer
    unless win_img.nil?
      correct_answer = URI.parse(win_img.attribute("src").value).query.split(/[=\.]/)[1]
      # puts "Dobre #{domain}" if correct_answer == answer
      # puts "Zle #{domain}" if correct_answer != answer
    end

    begin
      File.write(local_domain_cache_path(domain), File.read(local_answer_cache_path(correct_answer)))
    rescue Errno::ENOENT
    end

    @last_lives = lives

    [domain, correct_answer]
    puts "--- Answered #{domain}"
  end

  def train
    level = next_level!
    Kernel.exit if level.level >= 80
    if level.lives <= 1
      reset!
      level = next_level!
    end
    [level.level, level.lives, answer!(level.domain, level.answers.first.answer_hash, level.level)].flatten
  end

  def base_url
    "http://www.websupport.sk"
  end

  def match_url
    "/webmatch"
  end

  def answer_url(answer)
    "/webmatch?answ=#{answer}"
  end

  def screenshot_url(answer)
    "/images/domain.php?id=#{answer}.jpg"
  end

  def reset_url
    "/webmatch/reset"
  end
end

WM = Webmatch.new

class WebmatchAutomat < Sinatra::Base
  set :server, :thin

  get '/' do
    puts "+++ GET /"
    # @level = WM.next_level!
    @level = WM.next_level_with_cache!
    puts "--- GET /"
    haml :index
  end

  get '/reset' do
    WM.reset!
    redirect '/'
  end

  get '/answer' do
    puts "+++ GET /answer"
    answer = params[:answer]
    domain = params[:domain]
    lives = params[:lives]

    WM.answer!(domain, answer, lives)
    
    puts "--- GET /answer"
    redirect '/'
  end

  get '/screenshot' do
    answer = params[:answer]
    content_type 'image/jpeg'

    WM.get_screenshot(answer)
  end

  get '/train' do
    stream do |out|
      out << ""
      WM.reset!   
      while true
        level, lives, domain, correct_answer = WM.train
        out << sprintf("[Level: %d, Lives: %d] %s -> %s<br/>", level, lives, domain, correct_answer)
      end

    end
  end

  helpers do
  end

end
