Webmatch automat
================

Author
------

* Ahmed Al Hafoudh <alhafoudh@freevision.sk>, [www.freevision.sk](http://www.freevision.sk)
* Valter Martinek <martinek@freevision.sk>, [www.freevision.sk](http://www.freevision.sk)
* Matej Hlatký <hlatky@freevision.sk>, [www.freevision.sk](http://www.freevision.sk)

Screencast
----------

* [Video](https://dl.dropbox.com/u/350675/webmatch_hack.swf)

How others did it?
------------------

* [Vyhraj iPad s Websupportom](http://divnyblog.pokope.sk/blog/article/vyhraj-ipad-s-websupportom#.UECk2Wge48U)

How we did it?
-------------

* System emulates the /webmatch interface using HTTP GET requests, parses the output and presents cleaned interface to the user
* System uses any session id you give it
* System tracks answers, stores the correct ones (from answer response), caches the correct image
* On each level system compares the binary image data with the cache for picking the correct answer
* System can be automatically trained (reset match, answer wrong, get correct answer from response, repeat)
* After 2 hours training, system cached 5000 domain screenshots (1/10 of dataset)
* With 5000 domains cache, the success rate having cached answer is 8/10 (8 cached domains already known answers from 10 domains given) - Websupport server must have run out of entropy /dev/urandom
* System must have been slowed down with delay to emulate human clicking ;)

How to improve webmatch?
------------------------

1. Dont store state in session
2. Modify the image data on each request (may get to performance issues)
3. Rate limit the webserver
4. Ajaxify the game interface to have better response times and reduce server load
5. Strictly check response times (in combination with point 4.)
6. Use captcha to start new game level
7. More iPad's ;)

Tools used
----------

* ruby 1.9.3-p194
* some rubygems (look at Gemfile)
* [Burp Suite](http://www.portswigger.net/burp/)
* ngrep
* tcpdump
* Google Chrome

Installation
------------

    git clone https://bitbucket.org/freevision/webmatch-automat webmatch-automat
    cd webmatch-automat/
    mkdir -p cache/answers

    bundle install

Startup
-------

    export PHPSESSID=desired_session_id
    rackup

Play
----

    open http://localhost:4567/reset
    
Train
-----

    open http://localhost:4567/train

Rules
-----

Štatút súťaže „WebMatch“ je jediným záväzným dokumentom, ktorý upravuje podrobné pravidlá súťaže „WebMatch“ tak, aby boli presne určené podmienky súťaže.

### Organizátor a pôsobnosť Súťaže WebMatch

1. Súťaž s názvom „WebMatch“ (ďalej ako „Súťaž“), je organizovaná a uskutočňovaná spoločnosťou WebSupport, s.r.o., Staré grunty 12, 841 04 Bratislava, IČO: 36 421 928, zapísanou v obchodnom registri Okresného súdu Bratislava I., vložka číslo: 63270/B (ďalej len ako „WebSupport“ alebo „organizátor“).
2. Súťaž sa vzťahuje výlučne na územie Slovenskej, Českej a Maďarskej republiky.
3. Súťaž prebieha na websupport.sk/webmatch
Trvanie spotrebiteľskej Súťaže
4. Trvanie Súťaže je jeden týždeň, pričom si organizátor vyhradzuje právo na predĺženie tohto trvania, pokiaľ to uzná za vhodné. Termín začiatku Súťaže je 27. 8. 2012, termín ukončenia Súťaže je zatiaľ stanovený na 2. 9. 2012

### Pravidlá Súťaže
5. Do súťaže sa môže zapojiť každý občan s trvalým pobytom na území Slovenskej, Českej alebo Maďarskej republiky, ktorý splní podmienky Súťaže.
6. Zo súťaže sú vylúčení zamestnanci spoločnosti WebSupport, s.r.o. a ich blízke osoby v zmysle ustanovenia § 116 zákona č. 40/1964 Zb. Občiansky zákonník v platnom znení.
7. Osoby nespĺňajúce podmienky pre účasť v Súťaži, tzn. v prípade podozrenia o porušení hernej mechaniky, nebudú do Súťaže zaradené, resp. budú z hry vylúčené. Ak sa preukáže, že táto osoba sa aj napriek uvedenému stala výhercom, výhra sa neodovzdá. V takomto prípade nastupuje na miesto tohto výhercu ďalší súťažiaci podľa rozhodnutia organizátora Súťaže a stáva sa oprávneným výhercom.
8. Do Súťaže je hráč zaradený registráciou prostredníctvom svojho e-mailu alebo zadaním loginu do WebAdmina (v prípade klienta WebSupportu). Každý hráč je identifikovaný podľa unikátneho e-mailu / loginu.
9. Každý hráč má na začiatku Súťaže 3 životy, pričom za stratu života sa považuje nesprávne spojenie názvu domény s obrázkom (screenshotom) web stránky, ktorý k doméne prislúcha. Za každé správne priradenie názvu domény a k nej prislúchajúcemu obrázku, súťažiaci postupuje do ďalšieho levelu.
10. Pri strate života sa v Súťaži pokračuje od posledného úspešného levelu, tzn. začína sa od levelu, kde posledne „prišiel o život“.
11. Reakcia na priradenie názvu domény k obrázku webstránky je časovo limitovaná, pričom aj vypršanie časového limitu sa považuje za stratu života. Časový limit na priradenie názvu domény k obrázku sa s narastaním levelov skracuje (tzn. čím vyšší level, tým kratší čas na reagovanie).
12. Každé nové zobrazenie názvu domény je pokladané za nový level. Maximálny možný počet levelov je 100.
13. Každý level obsahuje niekoľko obrázkov webstránok, pričom počet týchto obrázkov sa postupne zväčšuje (tzn. čím vyšší level, tým väčší počet obrázkov)
14. Po strate všetkých troch životov, sa hra končí. Následne bude zosumarizovaný celkový počet bodov, ktoré hráč nahral. Hráč bude vyzvaný na zadanie vlastného nicku, pod ktorým budú verejne zverejnené výsledky všetkých hráčov. Na tomto základe bude možné sledovať rebríček hráčov priebežne počas celého trvania Súťaže. Rebríček je dostupný na websupport.sk/webmatch pod obrázkami webstránok.
15. Súťaž je možné hrať neobmedzene, pričom do úvahy sa berie najvyšší dosiahnutý level hráča.

### Výhry a systém ich prideľovania
16. WebSupport venuje do Súťaže jeden iPad2 (WiFi, 16 GB) a 50% zľavy na akýkoľvek hosting vo WebSupporte (custom hosting, unlimited hosting a multihosting) na obdobie jedného roka. 50% zľava sa nevzťahuje na cenu domény.
17. Celkový počet výhier v priebehu Súťaže je 101. Z toho jeden iPad + 50% zľava na webhosting pre hráča s najvyšším dosiahnutým levelom a 50% zľava pre ďalších 99 hráčov s najvyšším dosiahnutým levelom.
18. Výhry sú určené pre prvých 100 najlepších hráčov s najvyššie dosiahnutým levelom.
19. Prvý najlepší hráč, myslí sa tým hráč, ktorý sa odstal do najvyššieho levelu spomedzi všetkých zapojených súťažiacich za celé obdobie hry, získava iPad.
20. Zákazníci WebSupportu, tzn. hráči, ktorí sa prihlásili pod svojim loginom do WebAdmina, získavajú 2 – krát po 50% zľave na webhosting. Táto výhoda neuberá na počte 100-tich zliav, čiže platí, že ocenených bude presne 100 najlepších hráčov, pričom klienti WebSupportu budú mať dve 50% zľavy.
21. 50% zľavu je možné využiť do konca septembra 2012.
22. 50% zľavu je možné použiť len na novú službu, nie na existujúcu. Zľavy nie je možné kumulovať – v rámci jednej objednávky môže byť uplatnená iba jeden zľavový kód.
23. 50% zľava platí na obdobie jedného roka (12 mesiacov).
24. V prípade, že rovnaký najvyšší level dosiahnu dvaja alebo viacerí hráči, rozhodujúci bude čas, za ktorý sa k tomuto levelu hráč dostal (tzn. súťažiaci s najkratším časom na dosiahnutie najvyššieho levelu, získava iPad).
Kontaktovanie výhercov a právo na výhru
25. Každý výherca bude oboznámený o výhre e-mailom, ktorý zadal pri registrácií, po oficiálnom skončení Súťaže. Oznam o výhre zašle organizátor najneskôr do 1 týždňa po oficiálnom skončení Súťaže. V e-mailovej správe výhercovi oznámime aj spôsob uplatnenia 50% zľavy.
26. Meno a priezvisko výhercov bude zároveň zverejnené na profile WebSupportu na Facebooku.
27. V prípade výhry výhercom v zmysle §8 ods. 10 zákona č. 595/2003 Z. z. o daní z príjmu v platnom znení organizátor Súťaže WebSupport, s.r.o. oznámi výšku nepeňažnej výhry, aby výherca mohol predmetnú výšku správne zdaniť. Organizátor upozorňuje, že zdaneniu podliehajú len prijaté ceny alebo výhry v hodnote prevyšujúcej € 165,97. Bremeno zaplatenia dane z príjmu vzniká výhercovi prevzatím výhry v zmysle zákona č. 595/2003 Z.z. o daní v platnom znení.

### Osobitné ustanovenia

28. Organizátor Súťaže si vyhradzuje právo rozhodovať o všetkých otázkach týkajúcich sa tejto Súťaže, podľa vlastného uváženia a vyhradzuje si tiež právo z dôvodov hodných osobitného zreteľa kedykoľvek obmedziť, odložiť, prerušiť zmeniť alebo zrušiť Súťaž. V prípade akéhokoľvek sporu týkajúceho sa Súťaže, bude rozhodnutie organizátora Súťaže konečné a záväzné. Ak organizátor Súťaže zistí, že poskytol nefunkčný e-mail, bude oprávnený vylúčiť takéhoto výhercu zo Súťaže, pričom jeho nárok na výhru tým zaniká.

### Všeobecné záverečné ustanovenia

29. Súťažiaci svojou účasťou na spotrebiteľskej súťaži vyjadruje súhlas s jej pravidlami a štatútom súťaže, zaväzuje sa dodržiavať ich a zároveň dáva súhlas organizátorovi súťaže v prípade vyžrebovania použiť jeho e-mail na ich prezentáciu a spracovanie v propagačných materiáloch súťaže, a to bezplatne. Zároveň súhlasí s tým, že jeho nick, ktorý si sám zvolí na konci hry, bude použitý pri celkovom hodnotení dosiahnutých levelov. Tento rebríček bude prístupné každému hráčovi Súťaže na websupport.sk/webmatch.
30. Súťaž nie je hazardnou hrou v zmysle zákona č. 171/2005 Z.z. o hazardných hrách a o zmene doplnení niektorých zákonov v platnom znení.
31. Podrobné pravidlá Súťaže sú k dispozícií všetkým záujemcom na stránke websupport.sk/webmatch
32. V prípade akýchkoľvek sporov alebo nejasností, ktoré vyplývajú z tejto Súťaže, alebo ktoré vznikli v súvislosti s ňou, je vždy rozhodujúce konečné stanovisko organizátora.
33. Každý má právo nahliadnuť do tohto štatútu a požiadať o jeho fotokópiu. Štatút je k dispozícií v sídle organizátora Súťaže.
34. Účasťou v Súťaži prejavuje každý súťažiaci súhlas s pravidlami tejto Súťaže.

V Bratislave dňa: 27. 8. 2012